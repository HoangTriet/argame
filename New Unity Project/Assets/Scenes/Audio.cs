﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Audio : MonoBehaviour
{
    // Start is called before the first frame update
   public void SoundCase()
    {
        gameObject.GetComponent<Toggle>().isOn = !gameObject.GetComponent<Toggle>().isOn;
        AudioListener.pause = !AudioListener.pause;
    }
}
