﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PauseGame : MonoBehaviour
{
    public GameObject pausePanel;
    public void PauseTheGame()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
    }

    public void BacktoMain()
    {
        SceneManager.LoadScene("Welcome");
    }

    public void Sound()
    {
        gameObject.GetComponent<Toggle>().isOn = !gameObject.GetComponent<Toggle>().isOn;
        AudioListener.pause = !AudioListener.pause;
    }

    public void Resume()
    {
        Time.timeScale = 1;
        pausePanel.SetActive(false);
    }
}
