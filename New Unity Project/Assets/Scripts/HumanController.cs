﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class HumanController : MonoBehaviour
{
    Rigidbody rb;
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = gameObject.GetComponent<Animator>();
        rb.velocity = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        float x = CrossPlatformInputManager.GetAxis("Horizontal");
        float y = CrossPlatformInputManager.GetAxis("Vertical");

        Vector3 movement = new Vector3(x, 0, y);

        rb.velocity = movement * 4f;

        if(x != 0)
        {
            anim.SetBool("moving", true);
            if(x > 0)
            {
                transform.Translate(1f * Time.deltaTime, 0, 0);
            }
            else
            {
                transform.Translate(-1f * Time.deltaTime, 0, 0);
            }
        }
        if(y != 0)
        {
            anim.SetBool("moving", true);
            if(y > 0)
            {
                transform.Translate(0, 0, 1f * Time.deltaTime);
            }
            else
            {
                transform.Translate(0, 0, -1f * Time.deltaTime);
            }
        }
        if(x == 0 && y == 0)
        {
            anim.SetBool("moving", false);
        }

    }
}
